Dotty versions of [Classic Computer Science Problems in Python](https://www.manning.com/books/classic-computer-science-problems-in-python).

To run any example, type "sbt run" into a terminal at the root folder.

### Chapter 2 - Search
Largely faithful translations of the Python down to extensive use of mutable state.\
Top level functions are allowed in Dotty, making this cleaner than it would be in Scala 2.\
An enum (another new Dotty feature) is used for the Cell type in page 32, replacing Python strings.

### Chapter 3 - Constraints
Largely faithful translations of the Python down to extensive use of mutable state.

### Chapter 4 - Graphs
The Graph type is immutable in contrast to the Python implementation.\
The unweighted and weighted Graph types are merged into one class - the type of Edge (weighted or unweighted) is passed in as a constrained type parameter.\
Dotty's new representation of type classes using the `given` keyword is used here.

### Chapter 5 - Genetic Algorithms
Python functions such as mean, choices and compress are in Utils.scala.\
ListCompression doesn't show the same benefit as in the Python implementation (around 1 - 2% improvement on re-ordering the list).

### Chapter 6 - KMeans
TBD

### Chapter 7 - Neural Nets
There may be a bug, training success is less than in the book (though still much better than chance).
Use of a union type of singleton types in WineTest, to represent the three types of cultivar, as an Int that can only take values 1, 2, and 3.

### Chapter 8 - Adverserial Search
Complete