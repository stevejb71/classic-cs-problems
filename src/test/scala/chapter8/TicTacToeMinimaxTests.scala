package ccsp
package chapter8

import munit._

val E = None
val X = Some(Mark.X)
val O = Some(Mark.O)

// page: 161
class TicTacToeMinimaxTests extends FunSuite {
  test("easy position") {
    val positions = Vector(
      X, O, X,
      X, E, O,
      E, E, O
    )
    val board = TicTacToeGrid(positions, Mark.X)

    val move = findBestMove(board)

    assertEquals(move, 6)
  }

  test("block position") {
    val positions = Vector(
      X, E, E,
      E, E, O,
      E, X, O
    )
    val board = TicTacToeGrid(positions, Mark.X)

    val move = findBestMove(board)

    assertEquals(move, 2)
  }

  test("hard position") {
    val positions = Vector(
      X, E, E,
      E, E, O,
      O, X, E
    )
    val board = TicTacToeGrid(positions, Mark.X)

    val move = findBestMove(board)

    assertEquals(move, 1)
  }
}