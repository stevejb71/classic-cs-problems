package ccsp
package chapter5

import munit._

def sequence[A](values: A*): () => A = {
  val iter = values.iterator
  () => iter.next
}

class UtilsTests extends FunSuite:
  test("choices, various tests") {
    val weightedChoices = List(("1", 1.0), ("2", 5.0), ("3", 4.0))

    assertEquals(choices(weightedChoices, 4, sequence(0.0, 0.09, 0.1, 0.8)), Seq("1", "1", "2", "3"))
  }

  test("choose2 chooses two different values upto the given value") {
    val choices = sequence(1, 1, 1, 3)

    assertEquals((1, 3), choose2(choices))
  }