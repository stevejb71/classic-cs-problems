package ccsp
package chapter5

def MockSimpleRandom(doubles: Seq[Double] = Seq(), ints: Seq[Int] = Seq(), shuffle: String => Vector[Char] = s => s.reverse.toVector): SimpleRandom = {
  val doublesIter = doubles.iterator
  val nextDouble = () => doublesIter.next
  val intsIter = ints.iterator
  val nextInt = (_: Int) => intsIter.next
  SimpleRandom(nextDouble, nextInt, shuffle)
}