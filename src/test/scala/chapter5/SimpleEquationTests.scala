package ccsp
package chapter5

import munit._

class SimpleEquationTests extends FunSuite {
  private val eq = SimpleEquation(5, 1)

  test("mutate x down if two successive randoms are <= 0.5") {
    given random as SimpleRandom = MockSimpleRandom(doubles = Vector(0.5, 0.1))

    assertEquals(eq.mutated, SimpleEquation(4, 1))
  }

  test("mutate x up if first random <= 0.5, and second > 0.5") {
    given random as SimpleRandom = MockSimpleRandom(doubles = Vector(0.4, 0.6))

    assertEquals(eq.mutated, SimpleEquation(6, 1))
  }

  test("mutate y down if first random > 0.5, and second <= 0.5") {
    given random as SimpleRandom = MockSimpleRandom(doubles = Vector(0.51, 0.0))

    assertEquals(eq.mutated, SimpleEquation(5, 0))
  }

  test("mutate y up if first random > 0.5, and second > 0.5") {
    given random as SimpleRandom = MockSimpleRandom(doubles = Vector(0.51, 0.6))

    assertEquals(eq.mutated, SimpleEquation(5, 2))
  }
}