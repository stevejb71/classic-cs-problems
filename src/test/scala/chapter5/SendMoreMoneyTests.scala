package ccsp
package chapter5

import munit._

class SendMoreMoneyTests extends FunSuite:
  test("fitness of SEND MORE MONEY is 1") {
    given random as SimpleRandom = MockSimpleRandom(doubles = Seq(0.5, 0.1))

    val perfect = SendMoreMoney("MRNED  YSO")

    assertEquals(perfect.fitness, 1.0)
  }

  test("crossover") {
    given random as SimpleRandom = MockSimpleRandom(ints = Seq(1, 3))

    val p1 = SendMoreMoney("0123")
    val p2 = SendMoreMoney("1302")

    assertEquals(p1.crossover(p2), (SendMoreMoney("0132"), SendMoreMoney("3102")))
  }

  test("mutated swaps two letters around") {
    given random as SimpleRandom = MockSimpleRandom(ints = Seq(1, 3))

    val original = SendMoreMoney("0123")

    assertEquals(original.mutated, SendMoreMoney("0321"))
  }