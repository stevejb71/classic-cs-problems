package ccsp
package chapter5

case class SimpleEquation(val x: Int, val y: Int)

given simpleEquationChromosome(using random: SimpleRandom) as Chromosome[SimpleEquation]:
  override def randomInstance(): SimpleEquation = SimpleEquation(random.nextInt(100), random.nextInt(100))
  override def (eq: SimpleEquation).fitness: Double = {
    val x = eq.x
    val y = eq.y  
    6.0 * x - x.sqr + 4.0 * y - y.sqr
  }
  override def (eq: SimpleEquation).crossover(other: SimpleEquation): (SimpleEquation, SimpleEquation) = {
    val child1 = SimpleEquation(eq.x, other.y)
    val child2 = SimpleEquation(other.x, eq.y)
    (child1, child2)
  }
  override def (eq: SimpleEquation).mutated: SimpleEquation = {
    def mutateInt(n: Int): Int = n + (if (random.nextDouble() > 0.5) 1 else -1)
    val (x, y) = 
      if random.nextDouble() > 0.5
      then (eq.x, mutateInt(eq.y))
      else (mutateInt(eq.x), eq.y)
    SimpleEquation(x, y)
  }
  private def (x: Int).sqr: Int = x * x

@main def simpleEquationMain(): Unit = {
  given random as SimpleRandom = SimpleRandom()
  val ch = summon[Chromosome[SimpleEquation]]
  val population = Vector.fill(20)(ch.randomInstance())
  val result = runGeneticAlgorithm(population, 13.0, rndDouble = random.nextDouble, pickParents = Tournament(), log = println)
  println(result)
}