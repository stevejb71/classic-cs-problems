package ccsp
package chapter5

// implementation of Python's choices function - choose k items from a weighted list with replacement
def choices[A](weightedChoices: Seq[(A, Double)], numChoices: Int, randomDouble: () => Double): Seq[A] = {
  val cum = weightedChoices.map(_._2).scanLeft(0.0)(_ + _).drop(1)
  val scaled = cum.map(_ / cum.last)
  (1 to numChoices).map {_ =>
    val rnd = randomDouble()
    val idxMaybeNeg = scaled.indexWhere(_ > rnd)
    val elem = if (idxMaybeNeg == -1) weightedChoices.last else weightedChoices(idxMaybeNeg)
    elem._1
  }
}

def mean(xs: Seq[Double]): Double = xs.sum / xs.size

/** Chooses two different random ints */
def choose2(randomInt: () => Int): (Int, Int) = {
  val first = randomInt()
  @annotation.tailrec
  def second: Int = {
    val x = randomInt()
    if (x == first)
      second
    else
      x
  }
  (first, second)
}

case class SimpleRandom(nextDouble: () => Double, nextInt: Int => Int, shuffle: String => Vector[Char])
object SimpleRandom {
  def apply(): SimpleRandom = {
    val r = util.Random()
    SimpleRandom(() => r.nextDouble(), r.nextInt(_), (s: String) => r.shuffle(s).toVector)
  }
}

def compress(data: Array[Byte]): Array[Byte] = {  
  import java.io._
  import java.util.zip._
  val deflater = new Deflater()
  deflater.setInput(data);  
  val outputStream = new ByteArrayOutputStream(data.length);   
  deflater.finish();  
  val buffer = Array.ofDim[Byte](4096)
  while (!deflater.finished()) {  
    val count = deflater.deflate(buffer); // returns the generated code... index  
    outputStream.write(buffer, 0, count);   
  }  
  outputStream.close();  
  outputStream.toByteArray()
}  