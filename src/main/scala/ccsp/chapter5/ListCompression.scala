package ccsp
package chapter5

val PEOPLE = Vector.fill(50)(util.Random().nextString(5))

opaque type ListCompression = Vector[String]
def (lc: ListCompression).compressed: Array[Byte] = compress(lc.foldLeft("")(_ + _).getBytes)

given listCompression(using random: SimpleRandom) as Chromosome[ListCompression] {
  override def randomInstance(): ListCompression =  util.Random().shuffle(PEOPLE).toVector
  override def (lc: ListCompression).fitness = -lc.compressed.length.asInstanceOf[Double]
  override def (lc: ListCompression).crossover(other: ListCompression): (ListCompression, ListCompression) = {
    val (idx1, idx2) = choose2(() => random.nextInt(lc.size))
    val (l1, l2) = (lc(idx1), other(idx2))
    val child1 = lc.updated(lc.indexOf(l2), lc(idx2)).updated(idx2, l2)
    val child2 = other.updated(other.indexOf(l1), other(idx1)).updated(idx1, l1)
    (child1, child2)
  }
  override def (lc: ListCompression).mutated = {
    val (idx1, idx2) = choose2(() => random.nextInt(lc.size))
    lc.updated(idx1, lc(idx2)).updated(idx2, lc(idx1))
  }
}

@main def runListCompression(): Unit = {
  given random as SimpleRandom = SimpleRandom()
  val lc = summon[Chromosome[ListCompression]]
  val population = Vector.fill(10)(lc.randomInstance())
  val result = runGeneticAlgorithm(population, 
    threshold = 1.0, 
    maxGenerations = 10,
    mutationChance = 0.2,
    crossOverChance = 0.7,
    rndDouble = random.nextDouble, 
    pickParents = Tournament()
  )
  println(s"Initial list compressed size ${PEOPLE.compressed.length}")
  println(s"Optimised list compressed size ${result.compressed.length}")
}