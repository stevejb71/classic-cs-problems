package ccsp
package chapter5

import util.Random

// page: 96
trait Chromosome[C] {
  def randomInstance(): C
  def (c: C).fitness: Double
  def (c: C).crossover(other: C): (C, C)
  def (c: C).mutated: C
}