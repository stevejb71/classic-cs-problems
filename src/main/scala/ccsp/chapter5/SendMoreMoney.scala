package ccsp
package chapter5

import util.Random

// page: 105
opaque type SendMoreMoney = Vector[Char]
def SendMoreMoney(letters: String) = letters.toVector
def calculateWords(smm: SendMoreMoney): (Int, Int, Int, Int) = {
  val Seq(s, e, n, d, m, o, r, y) = "SENDMORY".map(smm.indexOf(_))
  val send = s * 1000 + e * 100 + n * 10 + d
  val more = m * 1000 + o * 100 + r * 10 + e
  val money = m * 10000 + o * 1000 + n * 100 + e * 10 + y
  val difference = (money - (send + more)).abs
  (send, more, money, difference)
}
def prettyPrint(smm: SendMoreMoney): String = {
  val (send, more, money, difference) = calculateWords(smm)
  s"$send + $more = $money | Difference: ${(send + more - money).abs}"
}

given sendMoreMoneyChromosome(using random: SimpleRandom) as Chromosome[SendMoreMoney] {
  override def randomInstance(): SendMoreMoney = {
    val letters = "SENDMORY  "
    random.shuffle(letters).toVector
  }
  override def (smm: SendMoreMoney).fitness: Double = {
    val (_, _, _, difference) = calculateWords(smm)
    1.0 / (difference + 1.0)
  }
  override def (smm: SendMoreMoney).crossover(other: SendMoreMoney): (SendMoreMoney, SendMoreMoney) = {
    val (idx1, idx2) = choose2(() => random.nextInt(smm.size))
    val (l1, l2) = (smm(idx1), other(idx2))
    val child1 = smm.updated(smm.indexOf(l2), smm(idx2)).updated(idx2, l2)
    val child2 = other.updated(other.indexOf(l1), other(idx1)).updated(idx1, l1)
    (child1, child2)
  }
  override def (smm: SendMoreMoney).mutated: SendMoreMoney = {
    val (idx1, idx2) = choose2(() => random.nextInt(smm.size))
    smm.updated(idx1, smm(idx2)).updated(idx2, smm(idx1))
  }
}

@main def sendMoreMoneyMain(): Unit = {
  given random as SimpleRandom = SimpleRandom()
  val ch = summon[Chromosome[SendMoreMoney]]
  val population = Vector.fill(1000)(ch.randomInstance())
  val result = runGeneticAlgorithm(population, 
    threshold = 1.0, 
    maxGenerations = 100,
    mutationChance = 0.2,
    crossOverChance = 0.7,
    rndDouble = random.nextDouble, 
    pickParents = Roulette()
  )
  println(prettyPrint(result))
}