package ccsp
package chapter5

import util.Random

trait SelectionType[C: Chromosome] {
  def apply(cs: Vector[C], rndDouble: () => Double): (C, C)
}

def runGeneticAlgorithm[C: Chromosome](
  population: Vector[C], 
  threshold: Double, 
  crossOverChance: Double = 0.7, 
  maxGenerations: Int = 100, 
  mutationChance: Double = 0.1, 
  pickParents: SelectionType[C],
  rndDouble: () => Double,
  log: String => Unit = println): C = {
  var best = population.maxBy(_.fitness)
  var currPopulation = population
  for (generation <- 0 to maxGenerations)
    // early exit if we beat threshold
    if (best.fitness >= threshold)
      return best
    log(s"Generation $generation Best ${best.fitness} Avg ${mean(currPopulation.map(_.fitness))}")
    currPopulation = reproduceAndReplace(currPopulation, crossOverChance, pickParents, rndDouble)
    currPopulation = mutate(mutationChance, currPopulation, rndDouble)
    val highest = currPopulation.maxBy(_.fitness)
    if (highest.fitness > best.fitness)
      best = highest
  best
}

private def mutate[C: Chromosome](mutationChance: Double, population: Vector[C], rndDouble: () => Double): Vector[C] = 
  population.map(c => if(rndDouble() < mutationChance) c.mutated else c)

private def reproduceAndReplace[C: Chromosome](population: Vector[C], crossOverChance: Double, pickParents: SelectionType[C], rndDouble: () => Double): Vector[C] = {
  val newPopulation = collection.mutable.ArrayBuffer[C]()
  while (newPopulation.size < population.size)
    val parents = pickParents(population, rndDouble)
    val nextGen = if (rndDouble() < crossOverChance)
      parents._1.crossover(parents._2)
    else
      parents
    newPopulation += nextGen._1
    newPopulation += nextGen._2
  // if we had an odd number, we'll have 1 extra, so remove
  if newPopulation.size > population.size
      newPopulation.dropInPlace(1)
  newPopulation.toVector
}

// page: 99
case class Roulette[C: Chromosome]() extends SelectionType[C]:
  def apply(population: Vector[C], rndDouble: () => Double): (C, C) = {
    val wheel = population.map(_.fitness)
    val Seq(a, b) = choices(population zip wheel, 2, rndDouble)
    (a, b)
  }

// page: 99
case class Tournament[C: Chromosome]() extends SelectionType[C]:
  def apply(population: Vector[C], rndDouble: () => Double): (C, C) = {
    val equalWeights = Array.fill(population.size)(1.0)
    val Seq(a, b) = choices(population zip equalWeights, population.size, rndDouble).sortBy(c => -c.fitness).take(2)
    (a, b)
  }