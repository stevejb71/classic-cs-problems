package ccsp
package chapter2

// page: 32
enum Cell(val repr: Char):
  case Empty extends Cell(' ')
  case Blocked extends Cell('X')
  case Start extends Cell('S')
  case Goal extends Cell('G')
  case Path extends Cell('*')

case class MazeLocation(val row: Int, val column: Int):
  // page: 44
  def euclideanDistanceSqr(other: MazeLocation): Int = (column - other.column).sqr + (row - other.row).sqr

  // page: 45
  def manhattanDistance(other: MazeLocation): Int = (column - other.column).abs + (row - other.row).abs

  private def (x: Int).sqr: Int = x * x

// page: 33
class Maze(rows: Int = 10, columns: Int = 10, sparseness: Double = 0.2, val start: MazeLocation = MazeLocation(0,0), val goal: MazeLocation = MazeLocation(9,9)):
  private val grid = Array.fill(rows, columns){if(util.Random.nextDouble < sparseness) Cell.Blocked else Cell.Empty}
  markStartAndEnd()

  override def toString(): String = {
    def rowToString(row: Array[Cell]): String = row.map(_.repr).mkString("")
    grid.map(rowToString).mkString("\n")
  }

  def reachedGoal(loc: MazeLocation): Boolean = loc == this.goal

  def successors(loc: MazeLocation): Vector[MazeLocation] = {
    val locations = collection.mutable.ArrayBuffer[MazeLocation]()
    if (loc.row + 1 < this.rows && this.grid(loc.row + 1)(loc.column) != Cell.Blocked) 
      locations += loc.copy(row = loc.row + 1)
    if (loc.row - 1 >= 0 && this.grid(loc.row - 1)(loc.column) != Cell.Blocked) 
      locations += loc.copy(row = loc.row - 1)
    if (loc.column + 1 < this.columns && this.grid(loc.row)(loc.column + 1) != Cell.Blocked) 
      locations += loc.copy(column = loc.column + 1)
    if (loc.column - 1 >= 0 && this.grid(loc.row)(loc.column - 1) != Cell.Blocked) 
      locations += loc.copy(column = loc.column - 1)
    locations.toVector
  }

  def mark(path: Seq[MazeLocation], value: Cell): Unit = {
    for (loc <- path)
      this.grid(loc.row)(loc.column) = value
    markStartAndEnd()
  }

  private def markStartAndEnd(): Unit = {
    grid(start.row)(start.column) = Cell.Start
    grid(goal.row)(goal.column) = Cell.Goal
  }