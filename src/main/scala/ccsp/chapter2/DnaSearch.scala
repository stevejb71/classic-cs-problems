package ccsp
package chapter2

// page: 25
enum Nucleotide:
  case A
  case C
  case G
  case T

// page: 26
val geneStr = "ACGTGGCTCTCTAACGTACGTACGTACGGGGTTTATATATACCCTAGGACTCCCTTT"

opaque type Codon = (Nucleotide, Nucleotide, Nucleotide)
opaque type Gene = List[Codon]

def stringToGene(s: String): Gene = {
  def charToNucleotide(ch: Char): Nucleotide = Nucleotide.valueOf(ch.toString)
  def stringToCodon(s: String): Codon = (charToNucleotide(s(0)), charToNucleotide(s(1)), charToNucleotide(s(2)))
  s.grouped(3)
    .filter(_.size == 3)
    .map(stringToCodon)
    .toList
}

// page: 27
val myGene = stringToGene(geneStr)

def linearContains(gene: Gene, keyCodon: Codon): Boolean = {
  for (codon <- gene)
    if (codon == keyCodon)
      return true
  return false
}