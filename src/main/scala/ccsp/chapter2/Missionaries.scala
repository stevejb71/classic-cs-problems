package ccsp
package chapter2

// page: 48, adapted
val MAX_NUM = 3

case class Bank(missionaries: Int, cannibals: Int):
  def isLegal = missionaries >= cannibals
  
case class MCState(west: Bank, boatOnWest: Boolean):
  def east = Bank(MAX_NUM - west.missionaries, MAX_NUM - west.cannibals)
  def reachedGoal = east.missionaries == MAX_NUM && east.cannibals == MAX_NUM
  def successors: Vector[MCState] = {
    val successors = collection.mutable.ListBuffer[MCState]()
    def (m: Option[MCState]).addIfLegal = m.foreach(successors += _)
    if (boatOnWest)
      mkMCState(west.missionaries - 2, west.cannibals, !this.boatOnWest).addIfLegal
      mkMCState(west.missionaries - 1, west.cannibals, !this.boatOnWest).addIfLegal
      mkMCState(west.missionaries, west.cannibals - 2, !this.boatOnWest).addIfLegal
      mkMCState(west.missionaries, west.cannibals - 1, !this.boatOnWest).addIfLegal
      mkMCState(west.missionaries - 1, west.cannibals - 1, !this.boatOnWest).addIfLegal
    else
      mkMCState(west.missionaries + 2, west.cannibals, !this.boatOnWest).addIfLegal
      mkMCState(west.missionaries + 1, west.cannibals, !this.boatOnWest).addIfLegal
      mkMCState(west.missionaries, west.cannibals + 2, !this.boatOnWest).addIfLegal
      mkMCState(west.missionaries, west.cannibals + 1, !this.boatOnWest).addIfLegal
      mkMCState(west.missionaries + 1, west.cannibals + 1, !this.boatOnWest).addIfLegal
    successors.toVector    
  }

def mkMCState(missionaries: Int, cannibals: Int, boatOnWest: Boolean): Option[MCState] = 
  if (missionaries >= 0 && missionaries <= MAX_NUM && cannibals >= 0 && cannibals <= MAX_NUM)
    Some(MCState(Bank(missionaries, cannibals), boatOnWest))
  else
    None

// page: 50
def displaySolution(path: Seq[MCState]): String = {
  path.sliding(2, 1).flatMap {
    case Seq(prev, curr) => 
      Vector(
        s"On the west bank there are ${prev.west.missionaries} missionaries and ${prev.west.cannibals} cannibals.",
        s"On the east bank there are ${prev.east.missionaries} missionaries and ${prev.east.cannibals} cannibals.",
        s"The boat is on the ${if (prev.boatOnWest) "west" else "east"} side.",
        if curr.boatOnWest
          s"${prev.east.missionaries - curr.east.missionaries} missionaries and ${prev.east.cannibals - curr.east.cannibals} cannibals moved from east to west bank"
        else
          s"${prev.west.missionaries - curr.west.missionaries} missionaries and ${prev.west.cannibals - curr.west.cannibals} cannibals moved from west to east bank",
        "\n",
      )
    case _ => ???
  }.mkString("\n")
}

@main def runMissionariesAndCannibals(): Unit = {
  val start = mkMCState(MAX_NUM, MAX_NUM, true).get
  val solution = bfs(start, _.reachedGoal, _.successors)
  val result = solution.map {
    s => displaySolution(nodeToPath(s))
  }.getOrElse("No solution found!")

  println(result)
}