package ccsp
package chapter2

import scala.collection.mutable.HashMap
import scala.collection.mutable.HashSet
import scala.collection.mutable.ListBuffer
import scala.collection.mutable.PriorityQueue
import scala.collection.mutable.Queue
import scala.collection.mutable.Stack
import scala.annotation.tailrec

def [A](a: A).some: Option[A] = Some(a)

case class Node[A](state: A, parent: Option[Node[A]], cost: Int, heuristic: Int)

given nodeOrd[A](using ord: Ordering[Int]) as Ordering[Node[A]] {
  def compare(n1: Node[A], n2: Node[A]) = ord.compare(n1.heuristic, n2.heuristic)
}

// page: 37
def dfs[A](initial: A, goalTest: A => Boolean, children: A => Vector[A]): Option[Node[A]] = {
  val frontier = Stack(Node(initial, None, 0, 0))
  val explored = HashSet(initial)
  while (!frontier.isEmpty)
    val currentNode = frontier.pop()
    val currentState = currentNode.state
    if(goalTest(currentState))
      return currentNode.some
    else
      children(currentState).filterNot(explored.contains).foreach {
        child =>
          explored.add(child)
          frontier.push(Node(child, currentNode.some, 0, 0))
      }
  None
}

// page: 37
def nodeToPath[A](node: Node[A]): Vector[A] = {
  val path = ListBuffer[A](node.state)
  var currentNode = node
  while(currentNode.parent.isDefined)
    currentNode = currentNode.parent.get
    path += currentNode.state
  path.toVector
}

// page: 41
def bfs[A](initial: A, goalTest: A => Boolean, children: A => Vector[A]): Option[Node[A]] = {
  val frontier = Queue(Node(initial, None, 0, 0))
  val explored = HashSet(initial)
  while (!frontier.isEmpty)
    val currentNode = frontier.dequeue()
    val currentState = currentNode.state
    if(goalTest(currentState))
      return currentNode.some
    else
      children(currentState).filterNot(explored.contains).foreach {
        child =>
          explored.add(child)
          frontier.enqueue(Node(child, currentNode.some, 0, 0))
      }
  None
}

// page: 46
def astar[A](initial: A, goalTest: A => Boolean, children: A => Vector[A], heuristic: A => Int): Option[Node[A]] = {
  val frontier = PriorityQueue(Node(initial, None, 0, heuristic(initial)))
  val explored = HashMap(initial -> 0)
  while (!frontier.isEmpty)
    val currentNode = frontier.dequeue()
    val currentState = currentNode.state
    if (goalTest(currentState))
      return currentNode.some
    else
      for (child <- children(currentState)) 
        val newCost = currentNode.cost + 1
        if (explored.get(child).map(_ >= newCost).getOrElse(true))
          explored.put(child, newCost)
          frontier.enqueue(Node(child, currentNode.some, newCost, heuristic(child)))
  None
}

