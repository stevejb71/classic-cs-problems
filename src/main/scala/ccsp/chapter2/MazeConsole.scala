package ccsp
package chapter2

def printSolution(maze: Maze, solution: Option[Node[MazeLocation]], searchType: String): Unit = {
  if (solution.isEmpty)
    println(s"No solution found using $searchType first search!")
  else
    println(s"Solution using $searchType first search")
    val path = nodeToPath(solution.get)
    maze.mark(path, Cell.Path)
    println(maze)
    maze.mark(path, Cell.Empty)
}

@main def run(): Unit = {
  val m = Maze()
  println(m)
  println()
  
  val dfsSolution = dfs(m.start, m.reachedGoal, m.successors)
  printSolution(m, dfsSolution, "depth")
  println()

  val bfsSolution = bfs(m.start, m.reachedGoal, m.successors)
  printSolution(m, bfsSolution, "breadth")
  println()

  val astarSolution = astar(m.start, m.reachedGoal, m.successors, m.goal.manhattanDistance)
  printSolution(m, astarSolution, "astar")
  println()
  }
