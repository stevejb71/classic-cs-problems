package ccsp
package chapter8

enum Mark:
  case X
  case O
given Eql[Mark, Mark] = Eql.derived

given Piece[Mark]:
  def (m: Mark).opposite: Mark = if m == Mark.X then Mark.O else Mark.X

type Move = 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 

class TicTacToeGrid(val positions: Vector[Option[Mark]], val turn: Mark):
  def row(index: Int): Vector[Option[Mark]] = Vector(positions(index * 3), positions(index * 3 + 1), positions(index * 3 + 2))
  def col(index: Int): Vector[Option[Mark]] = Vector(positions(index), positions(index + 3), positions(index + 6))
  def leftDiagonal: Vector[Option[Mark]] = Vector(positions(0), positions(4), positions(8))
  def rightDiagonal: Vector[Option[Mark]] = Vector(positions(2), positions(4), positions(6))
  def prettyPrint: String = (0 until 3).map(r => row(r).map(p => p.fold(".")(_.toString)).mkString).mkString("\n")

object TicTacToeGrid:
  val EMPTY = TicTacToeGrid(Vector.fill(9)(None), Mark.X)

given Board[TicTacToeGrid, Mark, Move]:
  def (b: TicTacToeGrid).turn = b.turn
  def (b: TicTacToeGrid).move(location: Move) = TicTacToeGrid(b.positions.updated(location, Some(b.turn)), b.turn.opposite)
  def (b: TicTacToeGrid).legalMoves = b.positions.zipWithIndex.filter((p, i) => p.isEmpty).map(_._2.asInstanceOf[Move])
  def (b: TicTacToeGrid).isWin = {
    def lineFilled(line: Vector[Option[Mark]]) = line(0).isDefined && line(0) == line(1) && line(0) == line(2)
    (0 until 3).map(b.row).exists(lineFilled) ||
    (0 until 3).map(b.col).exists(lineFilled) ||
    lineFilled(b.leftDiagonal) ||
    lineFilled(b.rightDiagonal)
  }
  def (b: TicTacToeGrid).evaluate(player: Mark) = {
    if (isWin(b))
      if turn(b) == player then -1.0 else 1.0
    else 
      0.0
  }

