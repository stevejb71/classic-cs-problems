package ccsp
package chapter8

trait Piece[P] {
  def (p: P).opposite: P
}

trait Board[B, P, M] {
  def (b: B).turn: P
  def (b: B).move(location: M): B
  def (b: B).legalMoves: Vector[M]
  def (b: B).isWin: Boolean
  def (b: B).isDraw: Boolean = !isWin(b) && legalMoves(b).isEmpty
  def (b: B).evaluate(player: P): Double
}