package ccsp
package chapter8

import annotation.tailrec

def getC4PlayerMove(board: Connect4Board): ColumnChoice = {
  println("Enter a legal square (0 - 6):")
  val value = io.StdIn.readInt()
  if value < 0 || value > 8
    getC4PlayerMove(board)
  else
    val move: ColumnChoice = value.asInstanceOf[ColumnChoice]
    if !board.legalMoves.contains(move)
    then getC4PlayerMove(board)
    else move
}

@main def playConnectFour(): Unit = {
  @tailrec
  def play(board: Connect4Board)(using b: Board[Connect4Board, Counter, ColumnChoice]): Unit = {
    var currBoard = board
    val humanMove: ColumnChoice = getC4PlayerMove(currBoard)
    currBoard = currBoard.move(humanMove)
    println(currBoard.prettyPrint)
    if (currBoard.isWin)
      println("Human wins!")
    else if (currBoard.isDraw)
      println("Draw!")
    else
      val computerMove: ColumnChoice = findBestMove[Connect4Board, Counter, ColumnChoice](currBoard, 2)
      println(s"Computer move is $computerMove")
      currBoard = currBoard.move(computerMove)
      println(currBoard.prettyPrint)
      if (currBoard.isWin)
        println("Computer wins!")
      else if (board.isDraw)
        println("Draw!")
      else
        play(currBoard)
  }
  println(Connect4Board.empty.prettyPrint)
  play(Connect4Board.empty)
}