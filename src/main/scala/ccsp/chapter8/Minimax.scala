package ccsp
package chapter8

import annotation.tailrec

// page: 159
def minimax[B, P: Piece, M](board: B, maximizing: Boolean, originalPlayer: P, maxDepth: Int)(using x: Board[B, P, M]): Double = {
  if (board.isWin || board.isDraw || maxDepth == 0)
    board.evaluate(originalPlayer)
  else 
    if (maximizing)
      board.legalMoves.foldLeft(Double.MinValue) {(eval, m) =>
        val result = minimax(board.move(m), false, originalPlayer, maxDepth - 1)
        Math.max(eval, result)
      }
    else // minimizing
      board.legalMoves.foldLeft(Double.MaxValue) {(eval, m) =>
        val result = minimax(board.move(m), true, originalPlayer, maxDepth - 1)
        Math.min(eval, result)
      }
}

// page: 169
def alphabeta[B, P: Piece, M](board: B, maximizing: Boolean, originalPlayer: P, maxDepth: Int, alpha: Double = Double.MinValue, beta: Double = Double.MaxValue)(using x: Board[B, P, M]): Double = {
  if (board.isWin || board.isDraw || maxDepth == 0)
    board.evaluate(originalPlayer)
  else
    val legalMoves = board.legalMoves     
    var i = 0
    var thisAlpha = alpha
    var thisBeta = beta
    var abort = false
    if (maximizing)
      while i < legalMoves.length && !abort do
        val result = alphabeta(board.move(legalMoves(i)), false, originalPlayer, maxDepth - 1, thisAlpha, thisBeta)
        thisAlpha = Math.max(result, thisAlpha)
        if thisBeta <= thisAlpha then
          abort = true
        i += 1
      thisAlpha
    else // minimizing
      while i < legalMoves.length && !abort do
        val result = alphabeta(board.move(legalMoves(i)), true, originalPlayer, maxDepth - 1, thisAlpha, thisBeta)
        thisBeta = Math.min(result, thisBeta)
        if thisBeta <= thisAlpha then
          abort = true
        i += 1
      thisBeta
}

def findBestMove[B, P: Piece, M](board: B, maxDepth: Int = 8)(using x: Board[B, P, M]): M = {
  var bestEval = Double.MinValue
  var bestMove = board.legalMoves(0)
  for m <- board.legalMoves do
    // replace with alphabeta for faster search
    val result = minimax(board.move(m), false, board.turn, maxDepth)
    if result > bestEval
    then 
      bestEval = result
      bestMove = m
  bestMove
}