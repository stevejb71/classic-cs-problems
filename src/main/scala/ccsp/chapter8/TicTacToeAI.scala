package ccsp
package chapter8

import annotation.tailrec

def getPlayerMove(board: TicTacToeGrid)(using b: Board[TicTacToeGrid, Mark, Move]): Move = {  
  println("Enter a legal square (0 - 8):")
  val value = io.StdIn.readInt()
  if value < 0 || value > 8
    getPlayerMove(board)
  else
    val move: Move = value.asInstanceOf[Move]
    if !board.legalMoves.contains(move)
    then getPlayerMove(board)
    else move
}

@main def playTicTacToeMain(): Unit = {
  @tailrec
  def play(board: TicTacToeGrid)(using b: Board[TicTacToeGrid, Mark, Move]): Unit = {
    var currBoard = board
    val humanMove: Move = getPlayerMove(currBoard)
    currBoard = currBoard.move(humanMove)
    if (currBoard.isWin)
      println("Human wins!")
    else if (currBoard.isDraw)
      println("Draw!")
    else
      val computerMove: Move = findBestMove[TicTacToeGrid, Mark, Move](currBoard)
      println(s"Computer move is $computerMove")
      currBoard = currBoard.move(computerMove)
      println(currBoard.prettyPrint)
      if (currBoard.isWin)
        println("Computer wins!")
      else if (board.isDraw)
        println("Draw!")
      else
        play(currBoard)
  }
  play(TicTacToeGrid.EMPTY)
}
