package ccsp
package chapter8

enum Counter:
  case B
  case R
  def toChar = if this == Counter.B then 'B' else 'R'

def [A] (xs: Vector[A]).modified(index: Int, f: A => A): Vector[A] = xs.updated(index, f(xs(index)))

given Piece[Counter]:
  def (m: Counter).opposite: Counter = if m == Counter.R then Counter.B else Counter.R

// page: 164
def generateSegments(numColumns: Int, numRows: Int, segmentLength: Int): Vector[Vector[(Int, Int)]] = {
  val verticalSegments = for
    c <- 0 until numColumns
    r <- 0 until numRows - segmentLength + 1
  yield for
    t <- 0 until segmentLength
  yield (c, r + t)

  val horizontalSegments = for
    c <- 0 until numColumns - segmentLength + 1
    r <- 0 until numRows
  yield for
    t <- 0 until segmentLength
  yield (c + t, r)

  val bottomLeftToTopRightDiagonalSegments = for
    c <- 0 until numColumns - segmentLength + 1
    r <- 0 until numRows - segmentLength + 1
  yield for
    t <- 0 until segmentLength
  yield (c + t, r + t)

  val bottomRightToTopLeftDiagonalSegments = for
    c <- 0 until numColumns - segmentLength + 1
    r <- segmentLength - 1 until numRows
  yield for
    t <- 0 until segmentLength
  yield (c + t, r - t)
  
  (verticalSegments ++ horizontalSegments ++ bottomLeftToTopRightDiagonalSegments ++ bottomRightToTopLeftDiagonalSegments).map(_.toVector).toVector
}

// page: 165
class Column(pieces: Vector[Counter]):
  def full: Boolean = pieces.size == Connect4Board.NUM_ROWS
  def push(item: Counter): Column = if full then throw Exception("Full") else Column(pieces :+ item)
  def get(index: Int): Option[Counter] = if index >= pieces.size then None else Some(pieces(index))

object Connect4Board {
  val NUM_ROWS = 6
  val NUM_COLUMNS = 7

  def empty: Connect4Board = Connect4Board(Vector.fill(NUM_COLUMNS)(Column(Vector.empty)), Counter.B)
}

class Connect4Board(val positions: Vector[Column], val turn: Counter):
  lazy val segments = generateSegments(Connect4Board.NUM_COLUMNS, Connect4Board.NUM_ROWS, 4)

  def countSegment(segment: Vector[(Int, Int)]): (Int, Int) = {
    segment.foldLeft((0, 0)) {case ((blackCount, redCount), (c, r)) => 
      val counter = positions(c).get(r)
      if counter == Some(Counter.B) then
        (blackCount + 1, redCount)
      else if (counter == Some(Counter.R))
        (blackCount, redCount + 1)
      else 
        (blackCount, redCount)
    }
  }

  def evaluateSegment(player: Counter)(segment: Vector[(Int, Int)]): Double = {
    val (blackCount, redCount) = countSegment(segment)
    if redCount > 0 && blackCount > 0 then
      0.0 // mixed segments are neutral
    else
      val count = Math.max(redCount, blackCount)
      val score = count match {
        case 2 => 1.0
        case 3 => 100.0
        case 4 => 1_000_000_000.0
        case _ => 0.0
      }
      val colour = if redCount > blackCount then Counter.R else Counter.B
      if colour == player then score else -score
  }

  def prettyPrint: String = {
    val lines = for
      r <- Connect4Board.NUM_ROWS - 1 to 0 by -1
    yield for 
      c <- 0 until Connect4Board.NUM_COLUMNS
    yield
      this.positions(c).get(r).fold('.')(_.toChar)
    val header = "|0123456|"
    (header +: lines.map(l => s"|${l.mkString}|")).mkString("\n")
  }

type ColumnChoice = 1 | 2 | 3 | 4 | 5 | 6 | 7

given Board[Connect4Board, Counter, ColumnChoice]:
  def (b: Connect4Board).turn = b.turn
  def (b: Connect4Board).move(location: ColumnChoice) = Connect4Board(b.positions.modified(location, _.push(b.turn)), b.turn.opposite)
  def (b: Connect4Board).legalMoves = b.positions.zipWithIndex.filter((c, _) => !c.full).map(_._2.asInstanceOf[ColumnChoice])
  def (b: Connect4Board).isWin = {
    b.segments.exists { segment => 
      val (blackCount, redCount) = b.countSegment(segment)
      blackCount == 4 || redCount == 4
    }
  }
  def (b: Connect4Board).evaluate(player: Counter) = b.segments.map(b.evaluateSegment(player)).sum

