package ccsp
package chapter7

class Layer(
  val previousLayer: Option[Layer],
  val numNeurons: Int,
  learningRate: Double,
  activationFun: Double => Double,
  activationFunDerivative: Double => Double,
  randomWeight: () => Double
):
  val neurons = (1 to numNeurons).map {i =>
    val randomWeights = previousLayer.map {l =>
        (1 to l.numNeurons).map(_ => randomWeight()).toArray
      }.getOrElse {
        Array[Double]()
      }
      Neuron(randomWeights, learningRate, activationFun, activationFunDerivative)
  }.toVector
  var outputCache = Vector.fill(numNeurons)(0.0)

  def outputs(inputs: Vector[Double]): Vector[Double] = {
    outputCache = previousLayer.map {l =>
      neurons.map(_.output(inputs))
    }.getOrElse {
      inputs
    }
    outputCache
  }

  // page: 139
  def calculateDeltasForOutputLayer(expected: Vector[Double]): Unit = {
    neurons.zipWithIndex.foreach {(n, i) =>
      n.delta = n.activationFunDerivative(n.outputCache) * (expected(i) - outputCache(i))
    }
  }

  // page: 140
  def calculateDeltasForHiddenLayer(nextLayer: Layer): Unit = {
    neurons.zipWithIndex.foreach {(n, i) => 
      val nextWeights = nextLayer.neurons.map(_.weights(i))
      val nextDeltas = nextLayer.neurons.map(_.delta)
      val sumWeightsAndDeltas = nextWeights.dotProduct(nextDeltas)
      n.delta = n.activationFunDerivative(n.outputCache) * sumWeightsAndDeltas
    }
  }