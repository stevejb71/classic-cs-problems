package ccsp
package chapter7

// page: 140
class Network(
  layerStructure: Vector[Int], 
  learningRate: Double, 
  activationFunc: Double => Double = sigmoid,
  activationFunDerivative: Double => Double = derivativeSigmoid,
  randomWeight: () => Double = util.Random.nextDouble
):
  if layerStructure.length < 3
  then throw IllegalArgumentException("Must have 3 or more layers, 1 input, 1 output, 1 hidden")

  private val layers: Vector[Layer] = {
    val layersBuffer = collection.mutable.ArrayBuffer[Layer]()
    layersBuffer += Layer(None, layerStructure(0), learningRate, activationFunc, activationFunDerivative, randomWeight)
    layerStructure.drop(1).zipWithIndex.foreach {(numNeurons, previous) =>
      val nextLayer = Layer(Some(layersBuffer(previous)), numNeurons, learningRate, activationFunc, activationFunDerivative, randomWeight)
      layersBuffer += nextLayer
    }
    layersBuffer.toVector
  }

  // page: 141
  def outputs(input: Vector[Double]): Vector[Double] = 
    layers.foldLeft(input){case (inputs, layer) =>
      layer.outputs(inputs)
    }

  // page: 141  
  /** Figure out each neuron's changes based on the errors of the output vs the expected outcome. */
  def backPropagate(expected: Vector[Double]): Unit = {
    val lastLayerIndex = layers.length - 1
    layers(lastLayerIndex).calculateDeltasForOutputLayer(expected)
    for i <- lastLayerIndex - 2 to 0 by -1 do
      layers(i).calculateDeltasForHiddenLayer(layers(i + 1))
  }

  def updateWeights(): Unit = {
    layers.tail.foreach {layer =>
      layer.neurons.foreach {neuron =>
        for w <- 0 until neuron.weights.length do
          neuron.weights(w) = neuron.weights(w) + (neuron.learningRate * (layer.previousLayer.get.outputCache(w) * neuron.delta))
      }
    }
  }

  // page: 142
  def train(inputs: Vector[Vector[Double]], expecteds: Vector[Vector[Double]]): Unit = {
    inputs.zipWithIndex.foreach {(xs, location) =>
      val ys = expecteds(location)
      val outs = outputs(xs)
      backPropagate(ys)
      updateWeights()
    }
  }

  def validate[A](inputs: Vector[Vector[Double]], expecteds: Vector[A], interpret: Vector[Double] => A): (Int, Int, Double) = {
    var correct = 0
    (inputs zip expecteds).foreach {(input, expected) => 
      val result = interpret(outputs(input))
      if result == expected then correct += 1
    }
    val percentage = correct.toDouble / inputs.length
    (correct, inputs.length, percentage)
  }