package ccsp
package chapter7

// union type of singleton types
type Cultivar = 1 | 2 | 3 

def (n: Int).toCultivar: Cultivar = if n >= 1 && n <=3 then n.asInstanceOf[Cultivar] else throw IllegalArgumentException(n.toString)

@main def wineTest(): Unit = {
  val wineOriginalData: Vector[Vector[Double]] = io.Source.fromFile("src/main/resources/ccsp/chapter7/wine.data").getLines
    .map(_.split(',').map(_.toDouble).toVector).toVector
  val wineClassifications = collection.mutable.ArrayBuffer[Vector[Double]]()
  val wineSpecies = collection.mutable.ArrayBuffer[Cultivar]()
  val wines = util.Random.shuffle(wineOriginalData)
  val wineParametersBuffer = collection.mutable.ArrayBuffer[Array[Double]]()
  var i = 0
  for wine <- wines do
    wineParametersBuffer += wine.tail.toArray
    val species: Cultivar = wine.head.toInt.toCultivar
    val classification = species match {
      case 1 => Vector(1.0, 0.0, 0.0)
      case 2 => Vector(0.0, 1.0, 0.0)
      case 3 => Vector(0.0, 0.0, 1.0)
    }
    wineClassifications += classification
    wineSpecies += species
  val wineParameters = wineParametersBuffer.toArray
  normalizeByFeatureScaling(wineParameters)

  val wineNetwork = Network(Vector(13, 7, 3), 0.9)

  // train over the first 150 wines in the data set 10 times
  println("Training...")
  val trainingSetSize = 150
  val wineTrainers = wineParameters.take(trainingSetSize).map(_.toVector).toVector
  val wineTrainerCorrects = wineClassifications.take(trainingSetSize).map(_.toVector).toVector
  for _ <- 0 until 10 do
    wineNetwork.train(wineTrainers, wineTrainerCorrects)

  // test over the last 10 of the irises in the data set
  println("Testing...")
  val wineTesters = wineParameters.drop(trainingSetSize).map(_.toVector).toVector
  val wineTesterCorrects = wineSpecies.drop(trainingSetSize).toVector
  val wineResults = wineNetwork.validate(wineTesters, wineTesterCorrects, interpretWineOutput)
  println(s"${wineResults(0)} correct of ${wineResults(1)} = ${wineResults(1)} * 100}%")
}

def interpretWineOutput(output: Vector[Double]): Cultivar = {
  val maxOutput = output.max
  (output.indexOf(maxOutput) + 1).toCultivar
}
