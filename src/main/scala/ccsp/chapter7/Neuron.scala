package ccsp
package chapter7

// page: 138
class Neuron(
  val weights: Array[Double],
  val learningRate: Double,
  activationFunc: Double => Double,
  val activationFunDerivative: Double => Double
  ) {
  var outputCache = 0.0
  var delta = 0.0

  def output(inputs: Vector[Double]): Double = {
    this.outputCache = inputs.dotProduct(this.weights.toIndexedSeq)
    this.activationFunc(outputCache)
  }
}
