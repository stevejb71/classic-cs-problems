package ccsp
package chapter7

enum Species:
  case Setosa
  case Versicolor
  case Virginica

object Species:
  def fromIrisDataString(s: String): Species = Species.valueOf(s.replace("Iris-", "").capitalize)

@main def irisTest(): Unit = {
  val irisOriginalData: Vector[Vector[String]] = io.Source.fromFile("src/main/resources/ccsp/chapter7/iris.data").getLines
    .map(_.split(',').toVector).toVector
  val irises = util.Random.shuffle(irisOriginalData)
  val irisParametersBuffer = collection.mutable.ArrayBuffer[Array[Double]]()
  val irisClassifications = collection.mutable.ArrayBuffer[Vector[Double]]()
  val irisSpecies = collection.mutable.ArrayBuffer[Species]()
  var i = 0
  for iris <- irises do
    val parameters = iris.take(4).map(_.toDouble)
    irisParametersBuffer += parameters.toArray
    val species = Species.fromIrisDataString(iris(4))
    val classification = species match {
      case Species.Setosa => Vector(1.0, 0.0, 0.0)
      case Species.Versicolor => Vector(0.0, 1.0, 0.0)
      case Species.Virginica => Vector(0.0, 0.0, 1.0)
    }
    irisClassifications += classification
    irisSpecies += Species.fromIrisDataString(iris(4))
  val irisParameters = irisParametersBuffer.toArray
  normalizeByFeatureScaling(irisParameters)

  val irisNetwork = Network(Vector(4, 6, 3), 0.3)

  // train over the first 140 irisises in the data set 50 times
  println("Training...")
  val trainingSetSize = 140
  val irisTrainers = irisParameters.take(trainingSetSize).map(_.toVector).toVector
  val irisTrainerCorrects = irisClassifications.take(trainingSetSize).map(_.toVector).toVector
  for _ <- 0 until 50 do
    irisNetwork.train(irisTrainers, irisTrainerCorrects)

  // test over the last 10 of the irises in the data set
  println("Testing...")
  val irisTesters = irisParameters.drop(trainingSetSize).map(_.toVector).toVector
  val irisTesterCorrects = irisSpecies.drop(trainingSetSize).toVector
  val irisResults = irisNetwork.validate(irisTesters, irisTesterCorrects, interpetOutput)
  println(s"${irisResults(0)} correct of ${irisResults(1)} = ${irisResults(1)} * 100}%")
}

def interpetOutput(output: Vector[Double]): Species = {
  val maxOutput = output.max
  val index = output.indexOf(maxOutput)  
  index match {
    case 0 => Species.Setosa
    case 1 => Species.Versicolor
    case 2 => Species.Virginica
    case _ => throw Error("Impossible!")
  }
}