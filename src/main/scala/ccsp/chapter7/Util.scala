package ccsp
package chapter7

// page: 136
def (xs: Seq[Double]).dotProduct(ys: Seq[Double]): Double = (xs zip ys).map((x, y) => x * y).sum

def (x: Double).sigmoid: Double = 1.0 / (1.0 + Math.exp(-x))

def (x: Double).derivativeSigmoid: Double = {
  val sigmoid = x.sigmoid
  sigmoid * (1 - sigmoid)
}

def normalizeByFeatureScaling(dataset: Array[Array[Double]]): Unit = {
  for colNum <- 0 until dataset(0).length do
    val column = dataset.map(_(colNum))
    val maximum = column.max
    val minimum = column.min
    for rowNum <- 0 until dataset.length do
      dataset(rowNum)(colNum) = (dataset(rowNum)(colNum) - minimum) / (maximum - minimum)
}