package ccsp
package chapter3

// page: 65
class SendMoreMoneyConstraint(letters: List[Char]) extends Constraint[Char, Int]:
  override val variables = letters
  override def satisfied(assignment: Assignment[Char, Int]): Boolean = {
    val calculator = this.calculator(assignment)
    // if duplicates, it's not a solution
    if (assignment.values.toSet.size < assignment.size)
      false
    // if all variables have been assigned, check it adds correctly
    else if (assignment.size == letters.size)
      val send = calculator("SEND")
      val more = calculator("MORE")
      val money = calculator("MONEY")
      send + more == money
    // else, no conflict
    else
      true
  }

  def calculator(assignment: Assignment[Char, Int])(letters: String): Int = {
    letters.map(assignment).reduceLeft  {
      case (acc, letter) => 10 * acc + letter
    }
  }

@main def sendMoreMoney(): Unit = {
  val letters = List('S', 'E', 'N', 'D', 'M', 'O', 'R', 'Y')
  val zeroToNine = (0 to 9).toVector
  // Allow 0 to 9 for all letters, except M which must be 1 - this means CSP doesn't find all 0 solutions.
  val possibleDigits = letters.map(l => if(l == 'M') l -> Vector(1) else l -> zeroToNine).toMap
  val csp = CSP(possibleDigits)
  csp.addConstraint(SendMoreMoneyConstraint(letters))
  println(csp.backTrackingSearch().getOrElse("No solution found!"))
}