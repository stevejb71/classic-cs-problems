package ccsp
package chapter3

import collection.mutable

type Domains[V, D] = Map[V, Vector[D]]

type Assignment[V, D] = Map[V, D]

// page: 54
trait Constraint[V, D] {
  def variables: List[V]
  def satisfied(assignment: Assignment[V, D]): Boolean
}

type Constraints[V, D] = Map[V, Vector[Constraint[V, D]]]

// page: 54 & others
class CSP[V, D](private val domains: Domains[V, D]) {
  private val constraints: Map[V, mutable.ArrayBuffer[Constraint[V, D]]] = domains.keys.map(d => (d, mutable.ArrayBuffer())).toMap

  def consistent(variable: V, assignment: Assignment[V, D]): Boolean = constraints(variable).forall(_.satisfied(assignment))

  def addConstraint(constraint: Constraint[V, D]): Unit = {
    for (variable <- constraint.variables) {
      if (!domains.contains(variable))
        throw IllegalArgumentException(s"Variable '$variable' in constraint but not in CSP")
      constraints(variable) += constraint
    }
  }

  def backTrackingSearch(assignment: Assignment[V, D] = Map()): Option[Assignment[V, D]] = {
    // search is complete if all variables are assigned.
    if (assignment.size == domains.size)
      Some(assignment)
    else
      // gets all the variables in the CSP but not in the assignment
      val unassigned = domains.view.filterKeys(!assignment.contains(_)).toMap
      // gets all possible domain values for the first unassigned variable.
      val (firstVariable, firstDomain) = unassigned.iterator.next
      for (value <- firstDomain)
        val localAssigment = assignment + (firstVariable -> value)
        if (consistent(firstVariable, localAssigment))
          val result = backTrackingSearch(localAssigment)
          // if we didn't find the result, we weill end up backtrackin g
          if (result.isDefined)
            return result
      return None
  }
}