package ccsp
package chapter3

enum Colour:
  case Red
  case Green
  case Blue

enum Place:
  case WesternAustralia
  case NorthernTerritory
  case SouthAustralia
  case Queensland
  case NewSouthWales
  case Victoria
  case Tasmania

// page: 58
class MapColouringConstraint(private val place1: Place, private val place2: Place) extends Constraint[Place, Colour] {
  override def variables = List(place1, place2)  
  override def satisfied(assignment: Assignment[Place, Colour]): Boolean = {
    val assignedColour1 = assignment.get(place1)
    val assignedColour2 = assignment.get(place2)
    // if either place not in the assignment, then it's not yet possible for their colours to be conflicting
    if (assignedColour1.isEmpty || assignedColour2.isEmpty)
      true
    else
      // satsified if the colours are different.
      assignedColour1 != assignedColour2
  }
}
// page: 59
@main def colourMap(): Unit = {
  val domains: Domains[Place, Colour] = Place.values.map(p => (p, Colour.values.toVector)).toMap
  val csp = CSP(domains)
  import Place._
  csp.addConstraint(MapColouringConstraint(WesternAustralia, NorthernTerritory))
  csp.addConstraint(MapColouringConstraint(WesternAustralia, SouthAustralia))
  csp.addConstraint(MapColouringConstraint(SouthAustralia, NorthernTerritory))
  csp.addConstraint(MapColouringConstraint(Queensland, NorthernTerritory))
  csp.addConstraint(MapColouringConstraint(Queensland, SouthAustralia))
  csp.addConstraint(MapColouringConstraint(Queensland, NewSouthWales))
  csp.addConstraint(MapColouringConstraint(NewSouthWales, SouthAustralia))
  csp.addConstraint(MapColouringConstraint(Victoria, SouthAustralia))
  csp.addConstraint(MapColouringConstraint(Victoria, NewSouthWales))
  csp.addConstraint(MapColouringConstraint(Victoria, Tasmania))

  val solution = csp.backTrackingSearch(Map())
  println(solution)
}