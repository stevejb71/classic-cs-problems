package ccsp
package chapter3

class QueensConstraint(override val variables: List[Int]) extends Constraint[Int, Int] {
  override def satisfied(assignment: Assignment[Int, Int]): Boolean = {
    // q1c = queen 1 column, q1r = queen 1 row
    for ((q1c, q1r) <- assignment)
      for (q2c <- (q1c + 1) to variables.length) 
        if (assignment contains q2c)
          val q2r = assignment(q2c)
          // same row?
          if q1r == q2r
            return false
          // same diagonal?
          if ((q1r - q2r).abs == (q1c - q2c).abs)
            return false
    true
  }
}


@main def queens(): Unit = {
  val columns = (1 to 8).toList
  val rows = columns.map(c => (c -> (1 to 8).toVector)).toMap
  val csp = CSP(rows)
  csp.addConstraint(QueensConstraint(columns))
  val solution = csp.backTrackingSearch().map(x => x.toList.sorted)
  println(solution.getOrElse("no solution found!"))
}