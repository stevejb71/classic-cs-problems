package ccsp
package chapter4

// page: 79
case class WeightedEdge(u: Int, v: Int, w: Int)
given Edge[WeightedEdge] {
  def (e: WeightedEdge).reversed = WeightedEdge(e.v, e.u, e.w)
  def (e: WeightedEdge).u = e.u
  def (e: WeightedEdge).v = e.v
}

def prettyPrintWeighted(g: Graph[_, WeightedEdge]): String = {
  (0 until g.vertexCount).map {i => 
    val vertex = g.vertices(i)
    val edges = g.edges(i).flatten
    s"$vertex -> [${edges.sortBy(_.w).map(e => s"(${g.vertices(e.v)},${e.w})").mkString(",")}]"
  }.mkString("\n")
}

@main def citiesByDistance(): Unit = {
  val connectedCities = List(
    ("Seattle", "Chicago", 1737),
    ("Seattle", "San Francisco", 678),
    ("San Francisco", "Riverside", 386),
    ("San Francisco", "Los Angeles", 348),
    ("Los Angeles", "Riverside", 50),
    ("Los Angeles", "Phoenix", 357),
    ("Riverside", "Phoenix", 307),
    ("Riverside", "Chicago", 1704),
    ("Phoenix", "Dallas", 887),
    ("Phoenix", "Houston", 1015),
    ("Dallas", "Chicago", 805),
    ("Dallas", "Atlanta", 721),
    ("Dallas", "Houston", 225),
    ("Houston", "Atlanta", 702),
    ("Houston", "Miami", 968),
    ("Atlanta", "Chicago", 588),
    ("Atlanta", "Washington", 543),
    ("Atlanta", "Miami", 604),
    ("Miami", "Washington", 923),
    ("Chicago", "Detroit", 238),
    ("Detroit", "Boston", 613),
    ("Detroit", "Washington", 396),
    ("Detroit", "New York", 482),
    ("Boston", "New York", 190),
    ("New York", "Philadelphia", 81),
    ("Philadelphia", "Washington", 123),
  )
  val unconnectedCityGraph = Graph(
    "Seattle", "San Francisco", "Los Angeles", "Riverside", "Phoenix", "Chicago", "Boston", "New York", "Atlanta", "Miami", "Dallas", "Houston", "Detroit", "Philadelphia", "Washington"
  )
  val cityGraph = connectedCities.foldLeft(unconnectedCityGraph){
    case (c, (u, v, d)) => c.addEdgeByVertices(u, v, (u, v) => WeightedEdge(u, v, d))
  }

  println(prettyPrintWeighted(cityGraph))
  println()
}
