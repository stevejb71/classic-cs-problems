package ccsp
package chapter4

import collection.mutable.ArrayBuffer
import collection.mutable.PriorityQueue

// page: 83
def totalWeight(edges: Seq[WeightedEdge]): Int = edges.map(_.w).sum

type MST = Vector[WeightedEdge]

// PriorityQueue requires a given Ordering.
given Ordering[WeightedEdge] {
  def compare(w1: WeightedEdge, w2: WeightedEdge): Int = w2.w - w1.w
}

// p84: Jarnik's algorithm
def mst[V](wg: Graph[V, WeightedEdge], start: Int = 0): MST = {
  val pq = PriorityQueue[WeightedEdge]()
  // holds the final MST
  val result = ArrayBuffer[WeightedEdge]()
  // where we've been
  val visited = Array.fill(wg.vertexCount)(false)
  def visit(index: Int): Unit = {
    // mark as visited
    visited(index) = true
    // add all edges coming from this index into the priority queue
    wg.edges(index).flatten.filterNot(e => visited(e.v)).foreach(e => pq += e)
  }
  visit(start)
  while (!pq.isEmpty)
    val edge = pq.dequeue()
    if (!visited(edge.v))
      result += edge
      visit(edge.v)
  result.toVector
}

def prettyPrintWeightedEdges(mst: Seq[WeightedEdge]): String = {
  val path = mst.map(e => s"${cityGraph.vertices(e.u)} ${e.w}> ${cityGraph.vertices(e.v)}").mkString("\n")
  s"$path\nTotal Weight: ${totalWeight(mst)}"
}

@main def runMST(): Unit = {
  val solution = mst(cityGraph)
  println(prettyPrintWeightedEdges(solution))
}