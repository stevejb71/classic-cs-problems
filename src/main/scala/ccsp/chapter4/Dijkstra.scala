package ccsp
package chapter4

import collection.mutable.HashMap
import collection.mutable.PriorityQueue
import collection.View.unfold 

class DijkstraNode(val vertex: Int, val distance: Double)

given Ordering[DijkstraNode]:
  def compare(n1: DijkstraNode, n2: DijkstraNode): Int = (n2.distance - n1.distance).toInt

// case class needed for unapply (in the main function)
case class DijkstraResult(distances: Vector[Option[Double]], paths: Map[Int, WeightedEdge])
def distancesToVertexMap[V](wg: Graph[V, WeightedEdge], distances: Vector[Option[Double]]): Map[V, Double] = {
  // loop over a tuple of (Option(distance), index)
  distances.zipWithIndex
    // if the option is populated, map to a tuple of vertex -> distance, else map to None
    .map{case (d, i) => d.map(wg.vertices(i) -> _)}
    // flatten the options (remove all Nones and convert Some(x) to x)
    .flatten
    // convert to a map
    .toMap
}
def pathMapToPath(start: Int, end: Int, paths: Map[Int, WeightedEdge]): Vector[WeightedEdge] = {
  unfold(paths(end)) {
    e => {
      if (e.u != start)
        Some((e, paths(e.u)))
      else
        None
    }
  }.toVector.reverse
}

// page: 89
def dijkstra[V](wg: Graph[V, WeightedEdge], root: V): DijkstraResult = {
  // find starting index
  val first = wg.vertices.indexOf(root)
  // distances are unknown at first
  val distances: Array[Double] = Array.fill(wg.vertexCount)(Double.PositiveInfinity)
  distances(first) = 0.0
  // how we got to each vertex
  val path = HashMap[Int, WeightedEdge]()
  val pq = PriorityQueue(DijkstraNode(first, 0.0))

  while (!pq.isEmpty)
    val u = pq.dequeue().vertex
    val distU = distances(u) // should already have seen it, and is not +infinity
    // look at every edge from the vertex in question
    for (we <- wg.edgesForIndex(u))
      val distV = distances(we.v)
      if (distV > we.w + distU)
        // update dist to this vertex
        distances(we.v) = we.w + distU
        // update the edge on the shortest path to this vertex
        path(we.v) = we
        // explore it soon
        pq.enqueue(DijkstraNode(we.v, we.w + distU))

  // hide +infinity distances from the external interface
  val distancesVec = distances.map(d =>
    if (d == Double.PositiveInfinity) None else Some(d)
  ).toVector
  DijkstraResult(distancesVec, path.toMap)
}

@main def runDijkstra(): Unit = {
  val start = "Los Angeles"
  val DijkstraResult(distances, paths) = dijkstra(cityGraph, start)
  val nameDistance = distancesToVertexMap(cityGraph, distances)
  println(s"Distances from $start")
  for ((key, value) <- nameDistance) 
    println(s"$key: $value")
  println()
  val end = "Boston"
  println(s"Shortest path from $start to $end")
  val shortestPath = pathMapToPath(cityGraph.vertices.indexOf(start), cityGraph.vertices.indexOf(end), paths)
  println(prettyPrintWeightedEdges(shortestPath))
}