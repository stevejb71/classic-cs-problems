package ccsp
package chapter4

// page: 71
/** An edge from u -> v */
trait Edge[E] {
  def (e: E).reversed: E
  def (e: E).u: Int
  def (e: E).v: Int
}

case class UnweightedEdge(u: Int, v: Int)
given Edge[UnweightedEdge] {
  def (e: UnweightedEdge).reversed = UnweightedEdge(e.v, e.u)
  def (e: UnweightedEdge).u = e.u
  def (e: UnweightedEdge).v = e.v
}

type Matrix[A] = Vector[Vector[Option[A]]]
def [A] (m: Matrix[A]).set(from: Int, to: Int, value: A): Matrix[A] = m.updated(from, m(from).updated(to, Some(value)))
def emptyMatrix[A](size: Int): Matrix[A] = Vector.fill(size)(Vector.fill(size)(None: Option[A]))

case class Graph[V, E: Edge](val vertices: Vector[V], edges: Matrix[E]):
  def vertexCount: Int = vertices.size
  def edgeCount: Int = 0
  def withVertex(v: V): Graph[V, E] = copy(vertices :+ v)
  /** Add an undirected graph edge (so an edge + its reverse) */
  def addEdge(e: E): Graph[V, E] = copy(edges = this.edges.set(e.u, e.v, e).set(e.v, e.u, e.reversed))
  def addEdgeByIndices(u: Int, v: Int, mkEdge: (Int, Int) => E) = mkEdge(u, v)
  def addEdgeByVertices(v1: V, v2: V, mkEdge: (Int, Int) => E): Graph[V, E] = {
    val u = vertices.indexOf(v1)
    val v = vertices.indexOf(v2)
    addEdge(mkEdge(u, v))
  }
  /** Finds all vertices that the vertex at the given index is connected to */
  def neighboursForIndex(index: Int): Vector[V] = {
    edges(index).map(_.map(e => vertices(e.v))).flatten
  }
  def neighboursForVertex(v: V): Vector[V] = neighboursForIndex(vertices.indexOf(v))
  def edgesForIndex(i: Int): Vector[E] = edges(i).flatten
  def edgesForVertex(v: V): Vector[E] = edgesForIndex(vertices.indexOf(v))

def prettyPrint(g: Graph[_, UnweightedEdge]): String = 
  (0 until g.vertexCount).map(i => s"${g.vertices(i)} -> [${g.neighboursForIndex(i).mkString(", ")}]").mkString("\n")

object Graph {
  def apply[V, E: Edge](vertices: V*): Graph[V, E] = Graph(vertices.toVector, emptyMatrix(vertices.size))
}

// page: 75
@main def cities(): Unit = {
    val connectedCities = List(
      ("Seattle", "Chicago"),
      ("Seattle", "San Francisco"),
      ("San Francisco", "Riverside"),
      ("San Francisco", "Los Angeles"),
      ("Los Angeles", "Riverside"),
      ("Los Angeles", "Phoenix"),
      ("Riverside", "Phoenix"),
      ("Riverside", "Chicago"),
      ("Phoenix", "Dallas"),
      ("Phoenix", "Houston"),
      ("Dallas", "Chicago"),
      ("Dallas", "Atlanta"),
      ("Dallas", "Houston"),
      ("Houston", "Atlanta"),
      ("Houston", "Miami"),
      ("Atlanta", "Chicago"),
      ("Atlanta", "Washington"),
      ("Atlanta", "Miami"),
      ("Miami", "Washington"),
      ("Chicago", "Detroit"),
      ("Detroit", "Boston"),
      ("Detroit", "Washington"),
      ("Detroit", "New York"),
      ("Boston", "New York"),
      ("New York", "Philadelphia"),
      ("Philadelphia", "Washington")
    )
    val unconnectedCityGraph = Graph(
      "Seattle", "San Francisco", "Los Angeles", "Riverside", "Phoenix", "Chicago", "Boston", "New York", "Atlanta", "Miami", "Dallas", "Houston", "Detroit", "Philadelphia", "Washington"
    )
    val cityGraph = connectedCities.foldLeft(unconnectedCityGraph){
      case (c, (u, v)) => c.addEdgeByVertices(u, v, UnweightedEdge)
    }

    println(prettyPrint(cityGraph))
    println()

    // page: 77
    import ccsp.chapter2.{bfs, nodeToPath}

    val bfsResult = bfs("Boston", _ == "Miami", cityGraph.neighboursForVertex)
    val printableResult = bfsResult.map(nodeToPath).getOrElse("No path found!")
    println("Path from Boston to Miami...")
    println(printableResult)
}


